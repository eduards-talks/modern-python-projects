<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

## Modern Python Projects
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->

----  ----

_Python is the second best programming language for everything._
Unknown

----  ----

## Outline

- Setup
- Linting
- Typing
- Testing
- Documentation
- Frameworks and Other Helpers
- Some Random Philosophy

Note:
This talk is a) opinionated and b) represents my best knowledge as of
June 2021 so take with grain of salt.

There will be code but do not worry if you do not get that.

Also all opinions...

----  ----

## Setup

- pyenv
- poetry
- src vs classical layout

Note:

Pyenv similar jabba. Language version management.

poetry: Dependency, packaging and venv management.

----

### src layout

- Tests will run against what is packaged instead of the dir tree of your project
- Same holds for your application
- Can not happen because `src` is not importable

For setup.py people (poetry auto-detects this):

```python
packages=find_packages(where="src"),
package_dir={"": "src"},
```

Note:

- You get import parity
- tox/nox work out of the box
- But: flat is better than nested - does not hold for data

----  ----

## Linting

- black
- flake8 [awesome](https://github.com/DmytroLitvinov/awesome-flake8-extensions)
- flake8-bugbear
- bandit
- pre-commit
- safety

Note:

- black: opinionated formatter
- flake8: extendable linter
- bandit (no toml yet)
- hook management for not only python
- safety (think dependency check)

----  ----

## Typing

- mypy (strictly annotations)
- pytype (inference)
- typeguard (runtime checking)
- flake8-annotations

Note:

- With annotations you will probably want to use `per-file-ignores`
- typeguard runtime performance impact, but finely tuneable i.e. function level checking en/disable

----  ----

## Testing

- pytest
- coverage
- pytest-mock
- nox
- invoke

Note:

- think of tox like a pneumatic nailgun. Very good but specific tool.
- nox uses python for configuration tox does not
- nox runs on a sessions concept and you can select which of them to run
- I just list invoke for completeness I personally do not use it

----  ----

## Documentation

- good old docstrings
- flake8-docstrings
- darglint
- xdoctest
- sphinx
- sphinx-autodoc-typehints
- sphinx-napoleon

Note:

- darglint lints signatures agains docstrings (and docstrings in general)
- xdoctest runs doctests (sphinx can also do that)
- napoleon pre-processes google-style docstrings to rst
- autodoc-typehints helps by adding types to the docs

----

### Docstrings

  ```python
    @dataclass
    class Fact:
      """
      Represents a fact .

      Attributes:
          header: A fact header containing some meta data.
          body: A json document containing the relevant data.
      """

      header: FactHeader
      body: Json
  ```

----

### Docstrings ctd

```python
    def serial_of(self, *, fact_id: UUID) -> Optional[str]:
        """
        Return the serial number of a fact given its UUID.

        Args:
          fact_id: The UUID of the fact you want the
            serial for.

        Returns:
          The serial or None.

        Raises:
          GrpcException: If something fails with the grpc communications.
        """

        msb, lsb = struct.unpack(">qq", fact_id.bytes)
        msg = MSG_UUID()
        msg.lsb = lsb
        msg.msb = msb
        res = self.client.serialOf(msg)

        if res.present:
            return str(res.serial)
        return None
```

----

### Doctests

```python
    def serial_of(self, *, fact_id: UUID) -> Optional[str]:
        """
        ...

        Raises:
          GrpcException: If something fails with the grpc communications.

        Example:
          >>> from pyfactcast.client.sync import client
          >>> client.serial_of(uuid4())
          None
        """
```

----  ----

## Frameworks and Other Helpers

Usualy python offers a lot of choice. To beginners that might not be the best
thing.

- [fastAPI](https://fastapi.tiangolo.com/)/[typer](https://typer.tiangolo.com/)
- [pydantic](https://pydantic-docs.helpmanual.io/)
- [desert](https://github.com/python-desert/desert)/[mashmallow](https://marshmallow.readthedocs.io/en/stable/)
- [asyncio](https://realpython.com/async-io-python/)
- [read the docs](https://readthedocs.org/)
- (test)[PyPi](https://pypi.org/)

----

# Sync/Async Divide

```python
import asyncio

async def async_function():
    print('Hello ...')
    await asyncio.sleep(1)
    print('... World!')

loop = asyncio.get_event_loop()
coroutine = async_function()
loop.run_until_complete(coroutine) # This blocks your sync execution.
```

Alternatively 2 threads/processes, one sync, one async and you are of to the races
of absolute insanity.

----  ----

## Demo Time

----  ----

## Philosophy

_Warning:_ Even stronger opinions ahead.

- [Zen of Python](https://www.python.org/dev/peps/pep-0020/)
- On Software Craft
- On FOSS

Note:
If this talk was not opinionated enough already here is some more.
Again my own not my employers. Also please feel free to disagree.

----

#### On Software Craft

- [Document your Tests](https://jml.io/pages/test-docstrings.html)
- [Markdown for Documentation is a Terrible Idea](https://www.ericholscher.com/blog/2016/mar/15/dont-use-markdown-for-technical-docs/)
- [Code is like Poetry](https://treyhunner.com/2017/07/craft-your-python-like-poetry/)
- There is no 10 minute change

Note:

- Do not use unnecessary words. Tell me why the behavior is desirable.
- No standards, loads of flavors, not extensible, almost no semantic meaning, not portable
- Readability counts. Be consistent, yes automate this.
- There always are surrounding things: tests, docs, other systems,...

----

#### On FOSS

- [One Of The Team](https://lukasa.co.uk/2016/04/One_Of_The_Team/)
- [The Social Contract of FOSS](https://snarky.ca/the-social-contract-of-open-source/)
- [Stop Caring](https://writing.jan.io/2017/03/06/sustainable-open-source-the-maintainers-perspective-or-how-i-learned-to-stop-caring-and-love-open-source.html)

Note:

- Be open and treat people like sensible adults. Make them feel their work is valuable.
Make them feel welcome.
- There are people out there that work for free. For you. Even if you are the maintainer,
respect that once your work is out there it is not _your thing_ any more. Authorial intent
does not matter!
- Do not stop caring about people or the project. Rather care about yourself so that you
can care about the project.

----  ----

# Thanks
